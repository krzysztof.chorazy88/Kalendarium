import "./App.css";
import { useState } from "react";
import { EntriesList } from "./EntriesList";
import { StyledButton, Container, InputDiv } from "./styles";
import { Note } from "./interfaces";

function App() {
  const [entry, setEntry] = useState<string>("");
  const [date, setDate] = useState<Date>(new Date());
  const [time, setTime] = useState({
    hour: date.getHours(),
    min: date.getMinutes(),
  });
  const [entries, setEntries] = useState<Array<Note>>([]);

  //ustawiam nowy stan dla input type text
  const onChangeInputHandler = (event: any) => {
    setEntry(event.target.value);
  };
  //ustawiam nowy stan dla inputy type date
  const onChangeDateHandler = (event: any) => {
    setDate(new Date(event.target.value));
    setTime({
      hour: date.getHours(),
      min: date.getMinutes(),
    });
  };

  const onSubmitHandler = (event: any) => {
    event.preventDefault();

    const newEntry = {
      entry,
      date,
      time,
    };
    const updatedEntries = [...entries, newEntry];
    console.log(updatedEntries);

    setEntries(updatedEntries);
  };

  return (
    <Container className="App">
      {/* to zaraz bedą inputy */}
      <form onSubmit={onSubmitHandler}>
        <InputDiv>
          <h3>
            <label htmlFor="text">Tu wpisz treść:</label>
          </h3>
          <input type="text" value={entry} onChange={onChangeInputHandler} />
        </InputDiv>
        <InputDiv>
          <h3>
            <label htmlFor="date">A tu wskaż date wpisu</label>
          </h3>
          <input
            id="date"
            type="date"
            value={date.toISOString()}
            onChange={onChangeDateHandler}
          />
        </InputDiv>
        <StyledButton type="submit">Add new entry</StyledButton>
      </form>
      <EntriesList notes={entries}/>
    </Container>
  );
}

export default App;
