function A() {
  const exampleProp = "example value";

  return (
    <B exampleProp={exampleProp} />
  );
}
function B(props) {
  return (
    <C exampleProp={props.exampleProp} />
  );
}
function C(props) {
  return (
    <div>{props.exampleProp}</div>
  );
}
