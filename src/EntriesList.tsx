import React from "react";
import { Entry } from "./Entry";
import { Note } from "./interfaces";
import { InputDiv } from "./styles";


interface EntriesListProps {
  notes:Note[]
}

export const EntriesList = (props:EntriesListProps) => {
  return (
    <InputDiv>
      <Entry note={props.notes}/>
    </InputDiv>
  );
};
