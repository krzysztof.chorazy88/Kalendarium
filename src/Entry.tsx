import React from "react";
import {Note } from "./interfaces";
import { StyledEntry, EntryParagraf } from "./styles";


//stwórz nowy interfejs dla każdego komponentu nawet jak on korzysta z innych jak tu
interface EntryProps {
  note: Note
}

export const Entry = (props: EntryProps) => {
  return (
    <div>
      <StyledEntry>{props.note.entry}</StyledEntry>
      
      <EntryParagraf>{props.note.date}</EntryParagraf>
      <EntryParagraf>
        {props.note.time}
      </EntryParagraf>
    </div>
  );
};
