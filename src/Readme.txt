== ELEMENTAR ==
1. Stwórz projekt, używając `create-react-app`.
2. Powiąż ten projekt z repozytorium w serwisie GitLab.
3. Niech strona główna wyświetla nazwę projektu zamiast domyślnej strony.
== BEGINNER ==
1. Wpis to tekst + data (stwórz na to interfejs).
2. Stwórz komponent na listę rzeczy, niech ona renderuje wszystkie wpisy.
3. Każdy wpis musi mieć widoczny tekst oraz datę.
== ADEPT ==
1. Pojedynczy wpis powinien mieć swój komponent, który będzie renderowany w komponencie listy.
2. Wpisy minione (data w przeszłości) powinny mieć szarą czcionkę (można dodać kursywę).
3. Niech wpis ma też godzinę, wyświetl ją osobno od daty.
== INTERMEDIATE ==
1. Formularz dodawania (input text + input data + input time).
2. Lista wpisów musi być stanem.
3. Dodawanie musi odbywać się poprzez propsa będącego funkcją.
== ADVANCED ==
1. Dodawanie musi przeprowadzać weryfikację, czy wszystkie inputy są niepuste, oraz data jest w przyszłości.
2. Niech wpis posiada również datę stworzenia, wyrenderuj ją, jednak by się odróżniała wizualnie.
3. Dodaj wpisowi informację czy jest on ważny. Na formularzu dodawania powinien być to checkbox, a wizualnie tekst powinien być pogrubiony.
== EXPERT ==
1. Dodaj możliwość usuwania elementów poprzez przypisany do nich przycisk.
2. Usuwanie nie usuwa całkiem, a przenosi do innego stanu na usunięte.
3. Usunięte renderują się poniżej formularza dodawania.
== IMPOSSIBLE ==
1. Dodaj do wpisów id.
2. Ta wartość nie powinna być wypełniana ręcznie, a każdorazowo przypisywana o 1 większa od poprzedniej (zaczynając od 1).
3. To id powinno się renderować wizualnie.
== ANDRZEJ ==
1. Dodaj możliwość edycji. Kliknięcie przycisku edycji obok wpisu zamienia wartości na inputy, a sam przycisk na inny: accept. Edytować można tylko nieusunięte.
2. Inputy domyślnie mają wypełnione wartości na podstawie edytowanego wpisu. Pamiętaj o informacji czy wpis jest ważny!
3. Akceptacja aktualizuje wpis i stan, by natychmiast zobaczyć efekt.